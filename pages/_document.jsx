import Document, { Head, Html, Main, NextScript } from 'next/document';
import React from "react";
import { ServerStyleSheet } from 'styled-components';
import Meta from "../src/components/Meta";

class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    
    const sheet = new ServerStyleSheet();
    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />),
    );
    const styleTags = sheet.getStyleElement();
    
    return { ...page, styleTags };
  }
  
  render() {
    const { title, description, url, image } = Meta;
    return (
      <Html lang="en">
        <Head>
          <link key={'ico'} rel="shortcut icon" type="image/x-icon" href="/static/logo.ico"/>
          <link key={'manifest'} rel="manifest" href="/static/manifest.json"/>
          <meta name="robots" content="all"/>
          
          <title>{title}</title>
          
          <meta key={'title'} name={'title'} content={title}/>
          <meta key={'description'} name={'description'} content={description}/>
          <meta key={'canonical'} name={'canonical'} content={url}/>
          
          <meta key={'og:url'} name={'og:url'} content={url}/>
          <meta key={'og:site_name'} name={'og:site_name'} content={title}/>
          <meta key={'og:title'} name={'og:title'} content={title}/>
          <meta key={'og:description'} name={'og:description'} content={description}/>
          
          <meta key={'og:type'} name={'og:type'} content={"website"}/>
          <meta key={'og:locale'} name={'og:locale'} content={"en_uk"}/>
          
          <meta key={'og:image'} name={'og:image'} content={image}/>
          <meta key={'og:image:alt'} name={'og:image:alt'} content={"logo"}/>
          
          <meta key={'twitter:creator'} name={'twitter:creator'} content={"@stillnotpedro"}/>
          
          <meta key={'twitter:title'} name={'twitter:title'} content={title}/>
          <meta key={'twitter:description'} name={'twitter:description'} content={description}/>
          
          <meta key={'twitter:image'} name={'twitter:image'} content={image}/>
          <meta key={'twitter:image:alt'} name={'twitter:image:alt'} content={"logo"}/>
          
          <meta name="theme-color" content="#fff"/>
          {this.props.styleTags}
        </Head>
        <body>
        <Main/>
        <NextScript/>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
