import App, { Container } from 'next/app';
import Head from 'next/head';
import React from 'react';
import Meta from "../src/components/Meta";

class MyApp extends App {
  
  render() {
    const { Component, pageProps } = this.props;
    const { title } = Meta;
    return (
      <Container>
        <Head>
          <title>{title}</title>
        </Head>
        <Component {...pageProps} />
      </Container>
    );
  }
}

export default MyApp;
