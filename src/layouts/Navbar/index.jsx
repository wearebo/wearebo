import React, { Fragment, useEffect, useState } from 'react';

import NavbarDesktop from './Desktop';
import NavbarMobile from './Mobile';

const Navbar = () => {
  const [showBackground, setShowBackground] = useState(false);
  
  const changeBackground = () => {
    const ref = document.getElementById('__next');
    ref.getBoundingClientRect().y > -81
      ? showBackground && setShowBackground(false)
      : !showBackground && setShowBackground(true);
  };
  
  useEffect(() => {
    window.addEventListener('scroll', changeBackground);
    return () => {
      window.removeEventListener('scroll', changeBackground);
    }
  });
  
  return (
    <Fragment>
      <NavbarDesktop background={showBackground}/>
      <NavbarMobile background={showBackground}/>
    </Fragment>
  );
};

export default Navbar;
