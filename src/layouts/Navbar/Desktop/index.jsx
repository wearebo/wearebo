import { useAmp } from 'next/amp';
import Link from "next/link";
import React from 'react';
import styled from "styled-components";

import MEDIA from '../../../../styles/media';
import theme from '../../../../styles/theme';
import { StyledNav } from '../styles';

const Wrapper = styled.div`
  justify-content: space-between;
  height: 80px;
  padding: 0 2rem;
  width: 100%;
  display: -webkit-flex;
  display: flex;
  align-items: center;
  ${MEDIA.mobile} {
    display: none;
  }
`;

const Links = styled.div`
  flex: 1;
  max-width: 500px;
  align-items: center;
  justify-content: flex-end;
  display: flex;
  white-space: nowrap;
`;

const Anchor = styled.a`
  display: block;
  text-decoration: none;
  font-weight: 500;
  color: ${theme.colorPrimary};
  text-transform: uppercase;
  font-size: .9rem;
  transition: color 200ms ease-out;
  margin-left: 1rem;
  cursor: pointer;
  
  &:hover {
    color: ${theme.colorNeutral2};
  }
`;

const Logo = styled.img`
  height: 88px;
`;

const NavbarDesktop = (props) => (
  <StyledNav background={props.background}>
    <Wrapper>
      <Link href={'/'}>
        <a>
          {useAmp() ?
            <amp-img src="/static/images/logoDark.svg" alt="wearebo logo dark" width="259" height="259"/> :
            <Logo src="/static/images/logoDark.svg" alt="wearebo logo dark"/>}
        </a>
      </Link>
      <Links>
        <Link href={'/clients'}><Anchor>Clients</Anchor></Link>
        {/*<Link href={'/blog'}><Anchor>Blog</Anchor></Link>*/}
        <Link href={'/contact'}><Anchor>Contact</Anchor></Link>
      </Links>
    </Wrapper>
  </StyledNav>
);

export default NavbarDesktop;
