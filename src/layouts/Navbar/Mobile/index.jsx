import { useAmp } from 'next/amp';
import Link from "next/link";
import React, {useState} from 'react';
import styled from "styled-components";
import MEDIA from '../../../../styles/media';
import theme from '../../../../styles/theme';

import MobileMenu from "../../MobileMenu";
import { StyledNav } from '../styles';

const Wrapper = styled.div`
  height: 50px;
  width: 100%;
  padding: 0 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  ${MEDIA.desktop} {
    display: none;
  }
`;

const Burger = styled.div`
  position: absolute;
  height: 50px;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 1rem;
  cursor: pointer;
`;

const StyledLink = styled.div`
  font-size: 20px;
  font-weight: bold;
  text-transform: uppercase;
  cursor: pointer;
  & > a {
    color: ${({ isVisible }) => {
      if (isVisible) {
        return theme.colorNeutral;
      } else {
        return theme.colorDark;
      }
    }};
  }
`;

const NavbarMobile = (props) =>  {
  const [isVisible, setIsVisible] = useState(false);
  
  const toggleMenu = () => {
    setIsVisible(!isVisible)
  };
  
    return (
      <StyledNav background={props.background} isMenuOpen={isVisible}>
        <Wrapper>
          <Burger onClick={toggleMenu}>
            {useAmp() ?
              <amp-img
                src={isVisible ? '../../../../static/images/close.svg' : '../../../../static/images/menu.svg'}
                alt={isVisible ? 'close menu' : 'open menu'}
                width='24'
                height='24'
              /> :
              <img
                src={isVisible ? '../../../../static/images/close.svg' : '../../../../static/images/menu.svg'}
                alt={isVisible ? 'close menu' : 'open menu'}
              />}
          </Burger>
          <MobileMenu isVisible={isVisible}/>
          <StyledLink isVisible={isVisible}>
            <Link href='/'>
              <a>wearebo</a>
            </Link>
          </StyledLink>
        </Wrapper>
      </StyledNav>
    );
}

export default NavbarMobile;
