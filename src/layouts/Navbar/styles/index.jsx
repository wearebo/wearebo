import styled from "styled-components";

import theme from "../../../../styles/theme";

export const StyledNav = styled.nav`
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 20;
  box-shadow: ${({ background, isMenuOpen }) => {
  if (background && !isMenuOpen) {
    return 'rgba(0, 0, 0, 0.07) 0 5px 15px';
  } else {
    return 'none';
  }
}};
  background: ${({ background, isMenuOpen }) => {
  switch (true) {
    case (isMenuOpen):
      return theme.colorPrimary;
    case (!background):
      return 'transparent';
    default:
      return theme.colorNeutral;
  }
}};
  transition: box-shadow 150ms ease 0s;
`;
