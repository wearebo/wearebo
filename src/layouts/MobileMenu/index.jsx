import Link from "next/link";
import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

import theme from '../../../styles/theme';
import SocialIcons from "../../components/SocialIcons";

const Div = styled.div`
  position: fixed;
  top: 50px;
  bottom: 0;
  width: 100%;
  z-index: 10;
  display: flex;
  justify-content: center;
  background-color: ${theme.colorPrimary};
  padding-top: 100px;
  padding-bottom: 50px;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Anchor = styled.a`
  display: block;
  text-decoration: none;
  font-weight: 500;
  color: #ffffff;
  text-transform: uppercase;
  font-size: 40px;
  transition: color 200ms ease-out;
  margin-bottom: 1rem;
  cursor: pointer;
`;

const MobileMenu = (props) => {
  if (!props.isVisible) {
    return null;
  }
  return ReactDOM.createPortal(
    <Div>
      <Wrapper>
        <div>
          <Link prefetch href={'/clients'}><Anchor>Clients</Anchor></Link>
          <Link prefetch href={'/contact'}><Anchor>Contact</Anchor></Link>
          {/*<Link href={'/blog'}><Anchor>Blog</Anchor></Link>*/}
        </div>
        <SocialIcons/>
      </Wrapper>
    </Div>,
    document.body
  );
};

export default MobileMenu;
