import Link from 'next/link';
import React from 'react';
import styled from 'styled-components';

import theme from '../../../styles/theme'
import SocialIcons from '../../components/SocialIcons';


const StyledFooter = styled.footer`
  position: relative;
  overflow: hidden;
  background: ${theme.colorPrimary};
  padding: 80px 32px;
  flex-shrink: 0;
`;

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
`;

const Links = styled.div`
  text-align: center;
  display: flex;
  margin: 0 auto 2rem;
  align-items: center;
  justify-content: center;
`;

const Anchor = styled.a`
  display: block;
  text-decoration: none;
  font-weight: 500;
  color: #DDE5E8;
  text-transform: uppercase;
  font-size: .9rem;
  transition: color 200ms ease-out;
  margin-left: 1rem;
  cursor: pointer;
`;

const Copy = styled.div`
  font-size: 13px;
  color: #8C95A1;
  text-align: center;
`;

const Footer = () => (
  <StyledFooter>
    <Container>
      <Links>
        <Link prefetch href={'/'}>
          <Anchor>Home</Anchor>
        </Link>
        <Link prefetch href={'/clients'}>
          <Anchor>Clients</Anchor>
        </Link>
        {/*<Link href={'/blog'}>*/}
        {/*  <Anchor>Blog</Anchor>*/}
        {/*</Link>*/}
        <Link prefetch href={'/contact'}>
          <Anchor>Contact</Anchor>
        </Link>
      </Links>
      <SocialIcons/>
      <Copy>
        Copyright © {new Date().getFullYear()} wearebo. All Rights Reserved.
      </Copy>
    </Container>
  </StyledFooter>
);

export default Footer;
