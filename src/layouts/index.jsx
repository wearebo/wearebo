import React, { Fragment } from 'react';
import { ThemeProvider } from 'styled-components';

import GlobalStyle from '../../styles/global';
import theme from '../../styles/theme';
import Footer from './Footer';
import Navbar from './Navbar';

const Layout = ({ children }) => (
  <ThemeProvider theme={theme}>
    <Fragment>
      <GlobalStyle/>
      <div className='content'>
        <Navbar/>
        {children}
      </div>
      <Footer/>
    </Fragment>
  </ThemeProvider>
);

export default Layout;
