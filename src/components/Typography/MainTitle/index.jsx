import React from 'react'

import Text from '../Text'

const MainTitle = ({ children, mode }) => {
  return (
    <Text
      tag='h1'
      color={mode === 'dark' ? 'colorNeutral' : 'colorDark'}
      weight='bold'
      size={80}
      sizeMobile={64}
      letterSpacing={-2.4}
      letterSpacingMobile={-1.44}
      lineHeight={88}
      lineHeightMobile={71}
    >
      {children}
    </Text>
  )
}

export default MainTitle
