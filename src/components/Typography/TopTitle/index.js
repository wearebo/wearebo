import React from 'react'
import styled from 'styled-components'

import theme from '../../../../styles/theme'

const Div = styled.div`
  font-size: 1rem;
  font-weight: 500;
  padding-top: .25rem;
  letter-spacing: .03em;
  line-height: 1.2;
  width: 100%;
  margin-bottom: .5rem;
  padding-left: 4rem;
  text-transform: uppercase;
  position: relative;
  color: ${({ mode, theme }) => {
    if (mode === 'dark') {
      return theme.colorNeutral
    } else {
      return theme.colorPrimary
    }
  }};
  &:before {
    position: absolute;
    left: 0;
    top: 50%;
    margin-top: -1px;
    height: 2px;
    width: 3rem;
    content: "";
    background: ${theme.colorSecondary};
  }
`

const TopTitle = ({ children, mode }) => (
  <Div mode={mode}>{children}</Div>
)

export default TopTitle
