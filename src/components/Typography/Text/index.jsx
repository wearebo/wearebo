import React from 'react'
import styled from 'styled-components'

import MEDIA from '../../../../styles/media'

export const StyledText = styled.p`
  margin: 0;
  color: ${({ color, theme }) => theme[color]};
  font-size: ${props => {
    if (typeof props.size === 'string') {
      return props.theme[props.size]
    }
    return props.size
  }}px;
    
  letter-spacing: ${props => props.letterSpacing}px;
  line-height: ${props => props.lineHeight}px;
  text-align: ${props => {
    return (props.align)
  }};
  
  font-weight: ${({ weight, theme }) => {
    switch (true) {
      case (weight === 'medium'): return theme.fontWeightMedium
      case (weight === 'bold'): return theme.fontWeightBold
      case (weight === 'light'): return theme.fontWeightLight
      default: return theme.fontWeightLight
    }
  }};
  color: ${props => {
    if (props.active) {
      return props.theme.colorPrimary
    }
  }};

  ${MEDIA.mobile} {    
    font-size: ${props => {
    if (typeof props.sizeMobile === 'string') {
      return props.theme[props.sizeMobile]
    }
    return props.sizeMobile
  }}px;
    letter-spacing: ${props => props.letterSpacingMobile || props.letterSpacing}px;
    line-height: ${props => props.lineHeightMobile || props.lineHeight}px;
  }
  
  text-decoration: ${({ underline }) => {
    if (underline) {
      return 'underline'
    }
  }};
  text-transform: ${({ upperCase }) => {
    if (upperCase) {
      return 'uppercase'
    }
  }};
  
  & p {
    margin: 0;
  }
`

const Text = (props) => {
  const { tag, children } = props
  const WithComponent = StyledText.withComponent(tag)
  return (
    <WithComponent {...props} className='styled-text'>
      {children}
    </WithComponent>
  )
}

export default Text
