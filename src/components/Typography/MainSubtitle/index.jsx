import React from 'react'

import Text from '../Text'

const MainSubtitle = ({children, mode}) => {
  return (
    <Text
      tag='h2'
      color={mode === 'dark' ? 'colorNeutral' : 'colorDark'}
      weight='bold'
      size={48}
      letterSpacing={-1.44}
      lineHeight={48}
    >
      {children}
    </Text>
  )
}

export default MainSubtitle
