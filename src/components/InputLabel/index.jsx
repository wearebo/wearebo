import React from 'react'
import styled from 'styled-components'

const Label = styled.label`
  display: block;
  text-transform: uppercase;
  color: rgb(37, 37, 39);
  font-weight: 700;
  margin-bottom: 0.5rem;
  font-size: 0.75rem;
`

const InputLabel = ({ children }) => {
  return (
    <Label>{children}</Label>
  )
}

export default InputLabel
