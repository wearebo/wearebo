import React from 'react'
import styled from 'styled-components'

const StyledArea = styled.textarea`
  -webkit-appearance: none;
  width: 100%;
  max-width: 70ch;
  display: block;
  font-size: 1rem;
  background: rgb(239, 239, 239);
  padding: 1rem;
  border-width: initial;
  border-style: none;
  border-color: initial;
  border-image: initial;
`

const TextArea = props => {
  return (
    <StyledArea {...props} />
  )
}

export default TextArea
