export default {
  title: 'wearebo',
  description: 'We are a strategy, design, and engineering firm.',
  url: "https://wearebo.co/",
  image: "https://wearebo.co/static/logo.png"
}
