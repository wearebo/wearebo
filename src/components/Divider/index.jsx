import React from 'react'
import styled from 'styled-components'

const Div = styled.div`
  margin-top: ${props => props.top}px;
  margin-bottom: ${props => props.bottom}px;
`

const Span = styled.span`
  margin-right: ${props => props.right}px;
  margin-left: ${props => props.left}px;
`

const Divider = (props) => {
  const { top, bottom } = props

  if (top || bottom) {
    return <Div {...props} />
  }
  return <Span {...props} />
}

export default Divider
