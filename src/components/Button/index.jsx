import React from 'react'
import styled from 'styled-components'

import theme from '../../../styles/theme'
import {disabledCss, primaryCss, secondaryCss} from "./styles";

export const Arrow = styled.span`
  margin-left: 10px;
`

const StyledButton = styled.button`
  color: #fff;
  background: ${theme.colorPrimary};
  border: 2px solid transparent;
  text-transform: uppercase;
  font-size: 13px;
  letter-spacing: 1.3px;
  font-weight: 700;
  padding: 12px 24px;
  cursor: pointer;
  transition: all 0.3s ease;
 
 ${({intent}) => {
  if (intent === 'primary') {
    return primaryCss
  } else {
    return secondaryCss
  }
}}
 
 ${({isDisabled}) => {
  if (isDisabled) {
    return disabledCss
  }
}}

`

const Button = React.forwardRef((props, ref) => {
  const { children } = props
  return (
    <StyledButton ref={ref} {...props}>
      {children}
      {props.type !== 'submit' && <Arrow aria-hidden="true">→</Arrow>}
    </StyledButton>
  )
})


export default Button
