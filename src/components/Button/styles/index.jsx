import { css } from 'styled-components'

import theme from '../../../../styles/theme'
import Arrow from '../'

export const primaryCss = css`
  color: ${({ theme }) => theme.colorNeutral};
  background: ${({ theme }) => theme.colorPrimary};
  &:hover {
    background-color: #fff;
    border: 2px solid ${theme.colorPrimary};
    color: ${theme.colorPrimary};
    
    & ${Arrow} {
      color: ${theme.colorPrimary};
    }
  }
`

export const secondaryCss = css`
  color: ${({ theme }) => theme.colorNeutral};
  background: ${({ theme }) => theme.colorSecondary};
  &:hover {
    background-color: ${theme.colorPrimary};
    border: 2px solid ${theme.colorSecondary};
    color: ${theme.colorSecondary};
    
    & ${Arrow} {
      color: ${theme.colorSecondary};
    }
  }
`

export const disabledCss = css`
  &:hover {
    background-color: ${theme.colorNeutral3};
    border: 2px solid ${theme.colorNeutral3};
    color: ${theme.colorNeutral};
  }
`
