import { useAmp } from 'next/amp';
import React from 'react';
import styled from "styled-components";

import theme from '../../../styles/theme';

const Wrapper = styled.div`
  margin-bottom: 2rem;
  color: #DDE5E8;
  display: flex;
  justify-content: center;
`;

const Anchor = styled.a`
  display: block;
  text-decoration: none;
  font-weight: 500;
  color: ${theme.colorNeutral};
  text-transform: uppercase;
  font-size: .9rem;
  transition: color 0.3s ease;
  margin-left: 1rem;
  cursor: pointer;
  
  &:hover path {
    fill: ${theme.colorSecondary};
  }
`;

const SocialIcons = () => (
  <Wrapper>
    <Anchor href={'https://twitter.com/stillnotpedro'}>
      {useAmp() ?
        <amp-img src='/static/images/twitter.svg' alt="Logo Twitter" width="20" height="20"/> :
        <img src='/static/images/twitter.svg' alt="Logo Twitter"/>}
    </Anchor>
    <Anchor href={'https://bitbucket.org/wearebo'}>
      {useAmp() ?
        <amp-img src='/static/images/bitbucket.svg' alt="Logo Bitbucket" width="20" height="20"/>
        :
        <img src='/static/images/bitbucket-icon.svg' alt="Logo Bitbucket"/>}
    </Anchor>
  </Wrapper>
);

export default SocialIcons;
