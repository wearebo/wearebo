import React, { Component } from 'react'
import styled from 'styled-components'

import theme from '../../../styles/theme'

const Div = styled.span`
  cursor: pointer;
  background-color: ${({ isActive }) => {
    if (isActive) {
      return theme.colorSecondary
    } else {
      return 'rgb(239, 239, 239)'
    }
}};
  padding: 10px;
  display: inline-block;
`

class Checkbox extends Component {
  state = { isActive: false }
  handleClick = () => {
    this.setState({
      isActive: !this.state.isActive
    }, this.props.handleChange(this.props.service, this.state.isActive))
  }
  render () {
    return (
      <Div onClick={this.handleClick} isActive={this.state.isActive}>
        {this.props.children}
      </Div>
    )
  }
}

export default Checkbox
