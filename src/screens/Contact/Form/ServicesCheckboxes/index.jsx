import React, { Fragment } from 'react';

import Checkbox from "../../../../components/Checkbox";
import Divider from "../../../../components/Divider";
import SERVICES from "../config/services";


const ContactFormServicesCheckboxes = (props) => {
  const handleChange = (service, isActive) => {
    let services = props.value
    if (!isActive) {
      services.push(service)
    } else {
      services = services.filter(item => item !== service)
    }
    props.setFieldValue('services', services)
  }
  return (
    <div>
      {SERVICES.map(service => (
        <Fragment key={service}>
          <Checkbox
            service={service}
            handleChange={handleChange}>
            {service}
          </Checkbox>
          <Divider right={10}/>
        </Fragment>
      ))}
    </div>
  )
}

export default ContactFormServicesCheckboxes;
