import { Form, Formik } from "formik";
// import dynamic from 'next/dynamic';
import React from 'react';

import Button from "../../../components/Button";
import Divider from "../../../components/Divider";
import Input from "../../../components/Input";
import InputLabel from "../../../components/InputLabel";
import TextArea from "../../../components/TextArea";
import ContactSchema from "./config/schema";
import ContactFormServicesCheckboxes from "./ServicesCheckboxes";

// const { Form, Formik } = dynamic(() => import('formik').then(mod => {
//   const { Form, Formik } = mod;
//   return { Form, Formik };
// }));


const ContactForm = () => (
  <Formik
    initialValues={{
      name: '',
      email: '',
      message: '',
      services: []
    }}
    validationSchema={ContactSchema}
    validateOnBlur={false}
    validateOnChange={false}
    onSubmit={(values, { setSubmitting }) => {
      console.log('hello', values);
      setSubmitting(false);
    }}
  >
    {({ handleChange, handleBlur, isValid, errors, setFieldValue, values }) => (
      <Form noValidate>
        <InputLabel>
          Name
        </InputLabel>
        <Input
          name='name'
          type='text'
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <span>{errors.name}</span>
        <Divider bottom={20}/>
        <InputLabel>
          Email</InputLabel>
        <Input
          name='email'
          type='email'
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <span>{errors.email}</span>
        <Divider bottom={20}/>
        <InputLabel>
          About your project
        </InputLabel>
        <TextArea
          rows={12}
          name='message'
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <span>{errors.message}</span>
        <Divider bottom={20}/>
        <InputLabel>
          Services (optional)
        </InputLabel>
        <ContactFormServicesCheckboxes
          setFieldValue={setFieldValue}
          value={values.services}
        />
        <Divider bottom={20}/>
        <Button
          intent='primary'
          type='submit'
          isDisabled={!isValid}
        >
          Submit
        </Button>
      </Form>
    )}
  </Formik>
);


export default ContactForm;
