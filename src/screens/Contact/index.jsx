import React from 'react';
import styled from "styled-components";

import dynamic from 'next/dynamic';

const Divider = dynamic(() => import("../../components/Divider"));
const MainTitle = dynamic(() => import("../../components/Typography/MainTitle"));
const Text = dynamic(() => import("../../components/Typography/Text"));
const TopTitle = dynamic(() => import("../../components/Typography/TopTitle"));
const Layout = dynamic(() => import("../../layouts"));
const ContactForm = dynamic(() => import("./Form"));

// import Divider from "../../components/Divider";
// import MainTitle from "../../components/Typography/MainTitle";
// import Text from "../../components/Typography/Text";
// import TopTitle from "../../components/Typography/TopTitle";

// import Layout from "../../layouts";
// import ContactForm from "./Form";

const Div = styled.div`
  margin-top: 80px;
`;

const Wrapper = styled.div`
  padding: 80px 32px;
  position: relative;
  overflow: hidden;
`;

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
`;

const TextWrapper = styled.div`
  margin-bottom: 96px;
`;

const ContactScreen = () =>
  <Layout>
    <Div>
      <Wrapper>
        <Container>
          <TextWrapper>
            <TopTitle>
              Contact us
            </TopTitle>
            <MainTitle>
              Let's talk
            </MainTitle>
            <Divider bottom={20}/>
            <Text
              tag='h2'
              weight='light'
              size={22}
              letterSpacing={-0.44}
            >
              Got a project? Give us a shout below and we'll be in touch.
            </Text>
          </TextWrapper>
          <ContactForm />
        </Container>
      </Wrapper>
    </Div>
  </Layout>;

export default ContactScreen;
