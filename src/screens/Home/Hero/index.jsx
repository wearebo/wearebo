import Link from "next/link";
import React from 'react';
import styled from 'styled-components';

import Button from "../../../components/Button";
import Divider from "../../../components/Divider";
import MainTitle from "../../../components/Typography/MainTitle";
import Text from "../../../components/Typography/Text";
import TopTitle from "../../../components/Typography/TopTitle";

const Div = styled.div`
  margin-top: 80px;
  background-image: radial-gradient(circle, #D7D7D7, #D7D7D7 1px, #FFF 1px, #FFF);
  background-size: 28px 28px;
`;

const Wrapper = styled.div`
  padding: 105px 30px 170px;
  position: relative;
  overflow: hidden;
`;

const ContentWrapper = styled.div`
  position: relative;
  overflow: hidden;
  max-width: 800px;
  margin: 0 auto;
`;

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
`;

const HomeHero = () => (
  <Div>
    <Wrapper>
      <Container>
        <ContentWrapper>
          <TopTitle>wearebo</TopTitle>
          <MainTitle>
            Building Digital.
          </MainTitle>
          <Divider bottom={20}/>
          <Text
            tag='h2'
            weight='light'
            size={40}
            sizeMobile={22}
            letterSpacing={-0.8}
            letterSpacingMobile={-0.45}
          >
            We're a strategy, design, and engineering studio.
          </Text>
          <Divider bottom={30}/>
          <Link
            prefetch
            href={'/contact'}
          >
            <Button intent='primary'>
              Work with us
            </Button>
          </Link>
        </ContentWrapper>
      </Container>
    </Wrapper>
  </Div>
);

export default HomeHero;
