import React from 'react'
import dynamic from 'next/dynamic';

const Layout = dynamic(() => import("../../layouts"));
const HomeHero = dynamic(() => import("./Hero"));
const HomeOurServices = dynamic(() => import("./OurServices"));
const HomeOurProcess = dynamic(() => import("./OurProcess"));
const HomeOurTools = dynamic(() => import("./OurTools"));
const HomeOurClients = dynamic(() => import("./OurClients"));
const HomeContact = dynamic(() => import("./Contact"));

// import HomeHero from "./Hero";
// import Layout from "../../layouts";
// import HomeOurServices from "./OurServices";
// import HomeOurProcess from "./OurProcess";
// import HomeOurTools from "./OurTools";
// import HomeOurClients from "./OurClients";
// import HomeContact from "./Contact";

const HomeScreen = () => (
  <Layout>
    <HomeHero/>
    <HomeOurServices/>
    <HomeOurProcess/>
    <HomeOurTools/>
    <HomeOurClients/>
    <HomeContact/>
  </Layout>
)

export default HomeScreen
