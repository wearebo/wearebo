import React from 'react'
import styled from 'styled-components'

import Text from "../../../components/Typography/Text";
import Divider from "../../../components/Divider";
import TopTitle from "../../../components/Typography/TopTitle";
import theme from "../../../../styles/theme";
import MainSubtitle from "../../../components/Typography/MainSubtitle";
import MEDIA from "../../../../styles/media";

const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
  background: ${theme.colorNeutral1};
  padding: 12rem 1rem;
`

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
`

const ContentWrapper = styled.div`
  margin: 0 auto;
  position: relative;
  max-width: 800px;
  color: #8C95A1;
`

const Content = styled.div`
  position: relative;
  color: ${theme.colorDark};
`

const Image = styled.img`
  max-height: 60px;
  max-width: 100%;
`

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  margin-top: 40px;
  margin-bottom: 35px;
  grid-column-gap: 30px;
  ${MEDIA.mobile} {
    grid-row-gap: 30px;
    grid-template-columns: 1fr 1fr;
  }
`;

const Cell = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
`;

const HomeOurClients = () => (
  <Wrapper>
    <Container>
      <ContentWrapper>
        <Content>
          <TopTitle>
            Who we work with
          </TopTitle>
          <MainSubtitle mode='light'>
            Clients & Partners
          </MainSubtitle>
          <Divider bottom={20}/>
          <Text
            tag='p'
            weight='light'
            size={22}
            lineHeight={30}
          >
            Some friends we've made along the way
          </Text>
          <Divider bottom={20}/>
          <Grid>
            <Cell>
              <a href='https://www.strydal.com/'>
                <Image src='../../../../static/images/strydal.svg'  alt={'strydal logo'} />
              </a>
            </Cell>
            <Cell>
              <a href='https://abihome.de/'>
                <Image src='../../../../static/images/abihome.svg'  alt={'abihome logo'} />
              </a>
            </Cell>
            <Cell>
              <a href='https://www.fundcalcs.com/'>
                <Image src='../../../../static/images/fundcalcs.png'  alt={'fundcalcs logo'}/>
              </a>
            </Cell>
            <Cell>
              <a href='https://www.wordans.com/'>
                <Image src='../../../../static/images/wordans.png'  alt={'wordans logo'} />
              </a>
            </Cell>
          </Grid>
        </Content>
      </ContentWrapper>
    </Container>
  </Wrapper>
)

export default HomeOurClients
