import Link from "next/link";
import React from 'react';
import styled from 'styled-components';

import theme from "../../../../styles/theme";
import Button from "../../../components/Button";
import Divider from "../../../components/Divider";
import Text from "../../../components/Typography/Text";
import MEDIA from '../../../../styles/media'

const Div = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  padding: 4rem 2rem;
  text-align: center;
  position: relative;
  background: ${theme.colorNeutral};
  flex-wrap: wrap;
`;

const Wrapper = styled.div`
  ${MEDIA.mobile} {
    margin-bottom: 30px;
  }
`;

const HomeContact = () => (
  <Div>
   <Wrapper>
     <Text
       tag='p'
       size={24}
       color='colorDark'
       weight="bold"
     >
       Got a project to discuss?
     </Text>
   </Wrapper>
    <Divider right={30}/>
    <Wrapper>
      <Link
        prefetch
        href={'/contact'}
      >
        <Button intent='primary'>
          Contact us
        </Button>
      </Link>
    </Wrapper>
  </Div>
);

export default HomeContact;
