import React from 'react';
import styled from 'styled-components';
import MEDIA from '../../../../styles/media';
import theme from "../../../../styles/theme";
import Divider from "../../../components/Divider";
import MainSubtitle from "../../../components/Typography/MainSubtitle";

import Text from "../../../components/Typography/Text";
import TopTitle from "../../../components/Typography/TopTitle";

const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
  background: ${theme.colorNeutral1};
  padding: 12rem 1rem;
`;

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
`;

const ContentWrapper = styled.div`
  margin: 0 auto;
  position: relative;
  max-width: 800px;
  color: #8C95A1;
`;

const Content = styled.div`
  position: relative;
  color: ${theme.colorDark};
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  margin-top: 20px;
  color: ${theme.colorPrimary};
  ${MEDIA.mobile} {
    grid-template-columns: 1fr;
  }
`;

const Row = styled.div`
  margin-bottom: 1rem;
`;

const HomeOurProcess = () => (
  <Wrapper>
    <Container>
      <ContentWrapper>
        <Content>
          <TopTitle>
            Our process
          </TopTitle>
          <MainSubtitle mode='light'>
            Unique products. Amazing results.
          </MainSubtitle>
          <Divider bottom={20} />
          <Text
            tag='p'
            weight='light'
            size={22}
            lineHeight={30}
          >
            Every client is unique so we build custom made solutions.
            From discovery to deployment, we work as a single unit with out clients and share the same goals.
            <br />
            We work in small and multidisciplinary teams to boost speed and efficiency and ensure on-time delivery.
          </Text>
          <Divider bottom={20}/>
          <Text
            tag='h3'
            weight='bold'
            size={24}
          >
            How we work
          </Text>
          <Grid>
            <div>
              <Row>In small teams</Row>
              <Row>With the right tools</Row>
              <Row>At a sustainable pace</Row>
            </div>
            <div>
              <Row>With flexibility</Row>
              <Row>Meeting clients regularly</Row>
              <Row>On exciting projects with amazing people</Row>
            </div>
          </Grid>
        </Content>
      </ContentWrapper>
    </Container>
  </Wrapper>
);

export default HomeOurProcess;
