const LOGOS = [
  {
    name: 'react',
    link: 'https://reactjs.org/'
  },
  {
    name: 'nodejs',
    link: 'https://nodejs.org/'
  },
  {
    name: 'redux',
    link: 'https://redux.js.org/'
  },
  {
    name: 'graphql',
    link: 'https://graphql.org'
  },
  {
    name: 'slack',
    link: 'https://slack.com/'
  },
  {
    name: 'nextjs',
    link: 'https://nextjs.org/'
  },
  {
    name: 'bitbucket',
    link: 'https://bitbucket.org/'
  },
  {
    name: 'nestjs',
    link: 'https://nestjs.com/'
  },
  {
    name: 'jira',
    link: 'https://www.atlassian.com/software/jira'
  },
  {
    name: 'figma',
    link: 'https://www.figma.com'
  }
]

export default LOGOS
