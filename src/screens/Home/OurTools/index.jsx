import React from 'react'
import styled from 'styled-components'

import Button from "../../../components/Button";
import Text from "../../../components/Typography/Text";
import Divider from "../../../components/Divider";
import TopTitle from "../../../components/Typography/TopTitle";
import theme from "../../../../styles/theme";
import MainSubtitle from "../../../components/Typography/MainSubtitle";
import LOGOS from './config/logos'
import Link from "next/link";

const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
  background: ${theme.colorPrimary};
  padding: 12rem 1rem;
`

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
`

const ContentWrapper = styled.div`
  margin: 0 auto;
  position: relative;
  max-width: 800px;
`

const Content = styled.div`
  position: relative;
  color: ${theme.colorNeutral2};
`

const StyledImage = styled.img`
  max-height: 32px;
  margin-right: 10px;
`

const HomeOurTools = () => (
  <Wrapper>
    <Container>
      <ContentWrapper>
        <Content>
          <TopTitle mode='dark'>
            Technology
          </TopTitle>
          <MainSubtitle mode='dark'>
            Tools of the trade
          </MainSubtitle>
          <Divider bottom={20} />
          <Text
            tag='p'
            weight='light'
            size={22}
            lineHeight={30}
          >
            We use the right tool for the right job, keeping our tech stack flexible and striving to deliver products
            that make sense for you. Some of our tools of choice are: React, React Native, NodeJs, Slack, Graphql, Redux.
          </Text>
          <Divider bottom={40} />

          {LOGOS.map(logo => (
            <a href={logo.link}>
              <StyledImage src={`/static/images/${logo.name}.svg`} alt={logo.name} />
            </a>
          ))}
          <Divider bottom={30} />
          <Link
            prefetch
            href={'/contact'}
          >
            <Button intent='secondary'>
              Work with us
            </Button>
          </Link>
        </Content>
      </ContentWrapper>
    </Container>
  </Wrapper>
)

export default HomeOurTools
