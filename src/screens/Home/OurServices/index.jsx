import Link from "next/link";
import React from 'react';
import styled from 'styled-components';
import MEDIA from '../../../../styles/media';
import theme from "../../../../styles/theme";

import Button from "../../../components/Button";
import Divider from "../../../components/Divider";
import MainSubtitle from "../../../components/Typography/MainSubtitle";
import Text from "../../../components/Typography/Text";
import TopTitle from "../../../components/Typography/TopTitle";
import SERVICES from './config/services';
import HomeOurServicesServiceColumn from "./ServiceColumn";

const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
  background: ${theme.colorPrimary};
  padding: 12rem 1rem;
`;

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
`;

const ContentWrapper = styled.div`
  margin: 0 auto;
  position: relative;
  max-width: 800px;
`;

const Content = styled.div`
  position: relative;
  color: ${theme.colorNeutral2};
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-row-gap: 50px;
  margin-top: 40px;
  margin-bottom: 35px;
  ${MEDIA.mobile} {
    grid-template-columns: 1fr;
  }
`;

const HomeOurServices = () => (
  <Wrapper>
    <Container>
      <ContentWrapper>
        <Content>
          <TopTitle mode='dark'>
            our services
          </TopTitle>
          <MainSubtitle mode='dark'>
            Strategy. Design. Engineering.
          </MainSubtitle>
          <Divider bottom={20} />
          <Text
            tag='p'
            weight='light'
            size={22}
            lineHeight={30}
          >
            wearebo transforms businesses by building superior data-driven experiences, products, and workflows
            based on strong creative, technology, and strategy
          </Text>
          <Grid>
            {SERVICES.map(service => (
              <HomeOurServicesServiceColumn service={service} key={service.title}/>
            ))}
          </Grid>
          <Link
            prefetch
            href={'/contact'}
          >
            <Button intent='secondary'>
              Get in touch
            </Button>
          </Link>
        </Content>
      </ContentWrapper>
    </Container>
  </Wrapper>
);

export default HomeOurServices;
