const SERVICES = [
  {
    title: 'Strategy',
    enum: [
      'Automation',
      'Data Science',
      'Business Consulting',
      'Project Planning',
      'Brand Strategy',
      'SEO',
    ]
  },
  {
    title: 'Design',
    enum: [
      'User Experience',
      'Interface Design',
      'Data Visualisation',
      'Product Design',
      'Technical Architecture',
      'Animation'
    ]
  },
  {
    title: 'Engineering',
    enum: [
      'Software Engineering',
      'Web Development',
      'Application Development',
      'Enterprise CMS',
      'Performance Auditing',
      'Devops',
    ]
  }
]

export default SERVICES
