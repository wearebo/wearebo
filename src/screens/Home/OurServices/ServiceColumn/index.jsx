import React from 'react'
import styled from 'styled-components'

import Text from "../../../../components/Typography/Text";
import Divider from "../../../../components/Divider";


const Row = styled.div`
  color: currentColor;
  margin-bottom: 1rem;
  position: relative;
  display: -webkit-flex;
  display: flex;
  align-items: center;
  padding-left: 33px;
  
  &:before {
    position: absolute;
    left: 0;
    top: 3px;
    border-radius: 50%;
    content: "";
    height: 11px;
    width: 11px;
    border: 1px solid;
    //border-color: currentColor;
  }
`


const HomeOurServicesServiceColumn = ({ service }) => (
  <div>
    <Text
    tag='h3'
    weight='bold'
    size={24}
    color='colorNeutral'
    >
      {service.title}
    </Text>
    <Divider bottom={20} />
    {service.enum.map((row, i) => (
      <Row key={`row-${i}`}>
        <Text
          tag='span'
        >
          {row}
        </Text>
      </Row>
    ))}
  </div>
)

export default HomeOurServicesServiceColumn
