import { useAmp } from 'next/amp';
import dynamic from 'next/dynamic';
import Link from "next/link";
import React from 'react';
import styled from "styled-components";
import MEDIA from "../../../styles/media";

const Layout = dynamic(() => import("../../layouts"));
const Divider = dynamic(() => import("../../components/Divider"));
const MainTitle = dynamic(() => import("../../components/Typography/MainTitle"));
const Text = dynamic(() => import("../../components/Typography/Text"));
const TopTitle = dynamic(() => import("../../components/Typography/TopTitle"));
const Button = dynamic(() => import("../../components/Button"));

// import Button from "../../components/Button";

// import Divider from "../../components/Divider";
// import MainTitle from "../../components/Typography/MainTitle";
// import Text from "../../components/Typography/Text";
// import TopTitle from "../../components/Typography/TopTitle";

// import Layout from "../../layouts";

const Div = styled.div`
  margin-top: 80px;
`;

const Wrapper = styled.div`
  padding: 80px 32px;
  position: relative;
  overflow: hidden;
`;

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
`;

const TextWrapper = styled.div`
  margin-bottom: 96px;
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  margin-top: 40px;
  margin-bottom: 35px;
  grid-column-gap: 30px;
  ${MEDIA.mobile} {
    grid-row-gap: 30px;
    grid-template-columns: 1fr 1fr;
  }
`;

const Cell = styled.div`
 display: flex;
 justify-content: center;
 align-items: flex-end;
`;

const ButtonWrapper = styled.div`
 display: flex;
 justify-content: center;
 margin-top: 60px;
`;

const Image = styled.img`
  max-height: 60px;
  max-width: 100%;
`;

const ClientsScreen = () =>
  <Layout>
    <Div>
      <Wrapper>
        <Container>
          <TextWrapper>
            <TopTitle>
              WHO WE WORK WITH
            </TopTitle>
            <MainTitle>
              Clients & Partners
            </MainTitle>
            <Divider bottom={20}/>
            <Text
              tag='h2'
              weight='light'
              size={22}
              letterSpacing={-0.44}
            >
              Some friends we've made along the way
            </Text>
          </TextWrapper>
          <div>
            <Grid>
              <Cell>
                <a href='https://www.strydal.com/'>
                  {useAmp() ?
                    <amp-img src='../../../static/images/strydal.svg' alt={'strydal logo'} width="92" height="30"
                             layout={'responsive'}/> :
                    <Image src='../../../static/images/strydal.svg' alt={'strydal logo'}/>}
                </a>
              </Cell>
              <Cell>
                <a href='https://abihome.de/'>
                  {useAmp() ?
                    <amp-img src='../../../static/images/abihome.svg' alt={'abihome logo'} width="52" height="41"
                             layout={'responsive'}/> :
                    <Image src='../../../static/images/abihome.svg' alt={'abihome logo'}/>}
                </a>
              </Cell>
              <Cell>
                <a href='https://www.fundcalcs.com/'>
                  {useAmp() ?
                    <amp-img src='../../../static/images/fundcalcs.png' alt={'fundcalcs logo'} width="271" height="100"
                             layout={'responsive'}/> :
                    <Image src='../../../static/images/fundcalcs.png' alt={'fundcalcs logo'}/>}
                </a>
              </Cell>
              <Cell>
                <a href='https://www.wordans.com/'>
                  {useAmp() ?
                    <amp-img src='../../../static/images/wordans.png' alt={'wordans logo'} width="858" height="199"
                             layout={'responsive'}/> :
                    <Image src='../../../static/images/wordans.png' alt={'wordans logo'}/>}
                </a>
              </Cell>
            </Grid>
            <ButtonWrapper>
              <Link href={'/contact'}>
                <a>
                  <Button intent='primary'>
                    Work with us
                  </Button>
                </a>
              </Link>
            </ButtonWrapper>
          </div>
        </Container>
      </Wrapper>
    </Div>
  </Layout>;

export default ClientsScreen;
