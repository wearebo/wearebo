# wearebo’s Website

[wearebo.co](https://wearebo.co)

## Development
- Install `yarn` from [yarnpkg.org](https://yarnpkg.org)
- Dev: `yarn dev` and open localhost:3000
- Build static: `yarn build` then `yarn export` to /out

## ▲
Every single push to master gets deployed to the **[Now](https://zeit.co/now)** Cloud automatically.  
Powered by **Next.js**, **MDX** and **▲ ZEIT Now**.

---

Website code open sourced under MIT.  
Content under CC BY-NC 4.0.
