import QUERIES from './config/queries'

const MEDIA = Object.keys(QUERIES).reduce((acc, query) => {
  acc[query] = `@media ${QUERIES[query]}`
  return acc
}, {})

export default MEDIA
