const theme = {
  colorPrimary: '#252527',
  colorSecondary: '#EF3340',
  colorNeutral: '#ffffff',
  colorNeutral1: '#F7FBFB',
  colorNeutral2: '#8C95A1',
  colorNeutral3: '#5F656D',
  colorDark: '#000000',
  fontWeightLight: 400,
  fontWeightMedium: 500,
  fontWeightBold: 700
};

export default theme;
