import { createGlobalStyle } from 'styled-components'

const globalStyles = createGlobalStyle`
  /* Common & global styles */
  * {
    box-sizing: border-box;
  }
  
  body,
  input,
  textarea,
  select,
  button {
    font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue, sans-serif;
    font-synthesis: none;
    -moz-font-feature-settings: 'kern';
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  
  body {
    margin: 0;
    height: 100%
  }
  
  html, #__next {
    height: 100%
  }
  
  #__next {
    display: flex;
    flex-direction: column;
  }
 
  a {
    text-decoration: none;
  }
  
  ul, menu, dir {
    padding: 0;
  }
  

textarea, select, input, button { outline: none; }

.content {
  flex: 1 0 auto;
}
`

export default globalStyles
