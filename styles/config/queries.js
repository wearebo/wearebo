const QUERIES = {
  mobile: 'screen and (max-width: 800px)',
  desktop: 'screen and (min-width: 801px)'
}

export default QUERIES
